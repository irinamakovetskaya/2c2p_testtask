﻿using System.Globalization;
using System.Linq;

namespace _2C2P.TestTask.BusinessFacade.Validators
{
	public static class ValidatorHelper
	{
		public static string[] GetCurrencyCodes()
		{
			return CultureInfo.GetCultures(CultureTypes.AllCultures)
				.Where(c => !c.IsNeutralCulture)
				.Select(culture => {
						try
						{
							return new RegionInfo(culture.Name);
						}
						catch
						{
							return null;
						}
					})
				.Where(p => p != null)
				.Select(p => p.ISOCurrencySymbol)
				.Distinct()
				.ToArray();
		}
	}
}