﻿using System.Globalization;
using System.Linq;
using _2C2P.TestTask.BusinessFacade.Models.XmlFileModel;
using FluentValidation;

namespace _2C2P.TestTask.BusinessFacade.Validators
{
	public class XmlTransactionValidator : AbstractValidator<XmlTransaction>
	{
		public XmlTransactionValidator()
		{
			var currencyCodes = ValidatorHelper.GetCurrencyCodes();
			RuleFor(p => p.PaymentDetails.CurrencyCode).Length(3).Must(p => currencyCodes.Contains(p));
		}
	}
}