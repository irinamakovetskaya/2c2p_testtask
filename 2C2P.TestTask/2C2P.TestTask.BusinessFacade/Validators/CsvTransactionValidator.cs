﻿using System.Linq;
using _2C2P.TestTask.Web.Models;
using FluentValidation;

namespace _2C2P.TestTask.BusinessFacade.Validators
{
	public class CsvTransactionValidator : AbstractValidator<CsvFileModel>
	{
		public CsvTransactionValidator()
		{
			var currencyCodes = ValidatorHelper.GetCurrencyCodes();
			RuleFor(p => p.CurrencyCode).Length(3).Must(p => currencyCodes.Contains(p));
		}
	}
}