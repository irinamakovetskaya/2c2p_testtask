﻿using System;
using System.Linq;
using LINQtoCSV;

namespace _2C2P.TestTask.BusinessFacade.Extensions
{
	public static class ExceptionExtensions
	{
		public static string GetAllMessages(this AggregatedException exception)
		{
			var messages = exception.m_InnerExceptionsList
				.Select(ex => ex.Message);
			return string.Join(Environment.NewLine, messages);
		}
	}
}
