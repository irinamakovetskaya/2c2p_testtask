﻿using System.Xml.Serialization;

namespace _2C2P.TestTask.BusinessFacade.Models.XmlFileModel
{
	[XmlRoot(ElementName = "Transactions")]
	public class XmlTransactions
	{
		[XmlElement(ElementName = "Transaction")]
		public XmlTransaction[] Items { get; set; }
	}
}