﻿using System;
using System.Xml.Serialization;
using _2C2P.TestTask.BusinessFacade.Models.Enums;

namespace _2C2P.TestTask.BusinessFacade.Models.XmlFileModel
{
	[XmlRoot(ElementName = "Transaction")]
	public class XmlTransaction
	{
		[XmlAttribute(AttributeName = "id")]
		public string TransactionId { get; set; }

		[XmlElement(ElementName = "TransactionDate")]
		public DateTime TransactionDate { get; set; }

		[XmlElement(ElementName = "PaymentDetails")]
		public PaymentDetails PaymentDetails { get; set; }

		[XmlElement(ElementName = "Status")]
		public TransactionStatus Status { get; set; }
	}
}