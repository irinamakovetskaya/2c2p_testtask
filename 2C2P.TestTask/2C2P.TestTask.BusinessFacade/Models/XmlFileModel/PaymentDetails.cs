﻿using System.Xml.Serialization;

namespace _2C2P.TestTask.BusinessFacade.Models.XmlFileModel
{
	[XmlRoot(ElementName = "PaymentDetails")]
	public class PaymentDetails
	{
		[XmlElement(ElementName = "Amount")]
		public decimal Amount { get; set; }

		[XmlElement(ElementName = "CurrencyCode")]
		public string CurrencyCode { get; set; }
	}
}