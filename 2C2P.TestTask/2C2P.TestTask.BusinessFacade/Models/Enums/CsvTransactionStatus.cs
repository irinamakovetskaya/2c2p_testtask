﻿namespace _2C2P.TestTask.BusinessFacade.Models.Enums
{
	public enum CsvTransactionStatus
	{
		Approved = 1,

		Failed = 2,

		Finished = 3
	}
}