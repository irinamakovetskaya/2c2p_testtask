﻿namespace _2C2P.TestTask.BusinessFacade.Models.Enums
{
	public enum TransactionStatus
	{
		Approved = 1,

		Rejected = 2,

		Done = 3
	}
}