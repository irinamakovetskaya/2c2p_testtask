﻿using _2C2P.TestTask.DataAccess.Entities;

namespace _2C2P.TestTask.BusinessFacade.Models
{
	public class ResponseItem
	{
		public ResponseItem(Transaction transaction)
		{
			Id = transaction.TransactionId;
			Payment = transaction.Amount.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + " " + transaction.CurrencyCode;
			Status = transaction.Status.Substring(0, 1);
		}

		public string Id { get; set; }

		public string Payment { get; set; }

		public string Status { get; set; }

	}
}