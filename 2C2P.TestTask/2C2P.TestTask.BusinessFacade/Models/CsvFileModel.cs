﻿using System;
using _2C2P.TestTask.BusinessFacade.Models.Enums;
using LINQtoCSV;

namespace _2C2P.TestTask.Web.Models
{
	[Serializable]
	public class CsvFileModel
	{
		[CsvColumn(FieldIndex = 1)]
		public string TransactionId { get; set; }

		[CsvColumn(FieldIndex = 2, OutputFormat = "#,##0.00")]
		public decimal Amount { get; set; }

		[CsvColumn(FieldIndex = 3)]
		public string CurrencyCode { get; set; }

		[CsvColumn(FieldIndex = 4, OutputFormat = "dd/MM/yyyy HH:mm:ss")]
		public DateTime TransactionDate { get; set; }

		[CsvColumn(FieldIndex = 5)]
		public CsvTransactionStatus Status { get; set; }
	}
}