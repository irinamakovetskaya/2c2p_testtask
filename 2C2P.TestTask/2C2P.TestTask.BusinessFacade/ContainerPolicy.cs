﻿using System.Web.Mvc;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using _2C2P.TestTask.BusinessFacade.Services;
using _2C2P.TestTask.DataAccess.Interfaces;
using _2C2P.TestTask.DataAccess.Services;
using Unity;
using Unity.AspNet.Mvc;

namespace _2C2P.TestTask.BusinessFacade
{
	public class ContainerPolicy
	{
		private readonly IUnityContainer CurrentContainer;

		public ContainerPolicy()
		{
			CurrentContainer = new UnityContainer();

			DependencyResolver.SetResolver(new UnityDependencyResolver(CurrentContainer));
		}

		public void Init()
		{
			InitBusinessServices();
			InitDataAccess();
		}

		private void InitBusinessServices()
		{
			CurrentContainer.RegisterType<ITransactionService, TransactionService>();
		}

		private void InitDataAccess()
		{
			CurrentContainer.RegisterType<ITransactionRepository, TransactionMongoRepository>();
		}
	}
}