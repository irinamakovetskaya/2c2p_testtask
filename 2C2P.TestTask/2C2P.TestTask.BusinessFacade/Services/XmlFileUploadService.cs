﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using _2C2P.TestTask.BusinessFacade.Models.XmlFileModel;
using _2C2P.TestTask.BusinessFacade.Validators;
using _2C2P.TestTask.DataAccess.Entities;

namespace _2C2P.TestTask.BusinessFacade.Services
{
	public class XmlFileUploadService : IFileUploadService
	{
		public List<Transaction> ParseFile(Stream stream)
		{
			var xml = new StreamReader(stream).ReadToEnd();
			List<Transaction> result = null;
			var document = new XmlDocument();
	 			document.LoadXml(xml);

			var serializer = new XmlSerializer(typeof(XmlTransactions));
			if (document.DocumentElement != null)
			{
				var xmlTransactions = (XmlTransactions)serializer.Deserialize(new XmlNodeReader(document.DocumentElement));
				ValidateModels(xmlTransactions.Items);

				result = xmlTransactions.Items.Select(
					p => new Transaction
							{
								TransactionId = p.TransactionId,
								Amount = p.PaymentDetails.Amount,
								CurrencyCode = p.PaymentDetails.CurrencyCode,
								TransactionDate = p.TransactionDate,
								Status = p.Status.ToString()
							}).ToList();
			}
			
			return result;
		}

		private static void ValidateModels(IList<XmlTransaction> items)
		{
			XmlTransactionValidator validator = new XmlTransactionValidator();
			var errors = items
				.Select(p => new { Id = p.TransactionId, ValidateResult = validator.Validate(p) })
				.Where(p => !p.ValidateResult.IsValid)
				.SelectMany(p => p.ValidateResult.Errors.Select(e => p.Id + ": " + e.ErrorMessage))
				.ToList();
			if (errors.Any())
			{
				throw new ApplicationException(string.Join(" ", errors));
			}
		}
	}
}