﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using _2C2P.TestTask.BusinessFacade.Models;
using _2C2P.TestTask.BusinessFacade.Models.Enums;
using _2C2P.TestTask.DataAccess.Entities;
using _2C2P.TestTask.DataAccess.Interfaces;

namespace _2C2P.TestTask.BusinessFacade.Services
{
	public class TransactionService : ITransactionService
	{
		private readonly ITransactionRepository _transactionRepository;

		public TransactionService(ITransactionRepository transactionRepository)
		{
			_transactionRepository = transactionRepository;
		}

		public async Task<string> UploadAsync(HttpPostedFileBase file)
		{
			string result = null;
			
			if (file == null || file.ContentLength == 0)
			{
				result = "File is empty";
			}
			else if (file.ContentLength > (1024 * 1024))
			{
				result = "File size is max 1 MB";
			}
			else
			{
				var extension = file.FileName.Substring(file.FileName.Length - 4, 4);
				IFileUploadService fileService = null;
				switch (extension)
				{
					case ".xml":
						fileService = new XmlFileUploadService();
						break;
					case ".csv":
						fileService = new CsvFileUploadService();
						break;
					default:
						result = "Unknown format";
						break;
				}

				List<Transaction> transactions = fileService?.ParseFile(file.InputStream);
				if (transactions != null && transactions.Count > 0)
				{
					await _transactionRepository.AddAsync(transactions);
				}
			}

			return result;
		}
		
		public async Task<List<ResponseItem>> GetByCurrency(string currencyCode)
		{
			var transactions = await _transactionRepository.Find(p => p.CurrencyCode == currencyCode);
			return transactions.Select(p => new ResponseItem(p)).ToList();
		}

		public async Task<List<ResponseItem>> GetByDateRange(DateTime dateFrom, DateTime dateTo)
		{
			var nextDateTo = dateTo.AddDays(1);
			var transactions = await _transactionRepository.Find(p => p.TransactionDate >= dateFrom && p.TransactionDate < nextDateTo);
			return transactions.Select(p => new ResponseItem(p)).ToList(); 
		}

		public async Task<List<ResponseItem>> GetByStatus(TransactionStatus status)
		{
			var str = status.ToString();
			var transactions = await _transactionRepository.Find(p => p.Status == str);
			return transactions.Select(p => new ResponseItem(p)).ToList(); 
		}
	}
}