﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using _2C2P.TestTask.BusinessFacade.Extensions;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using _2C2P.TestTask.BusinessFacade.Models.Enums;
using _2C2P.TestTask.BusinessFacade.Validators;
using _2C2P.TestTask.DataAccess.Entities;
using _2C2P.TestTask.Web.Models;
using LINQtoCSV;

namespace _2C2P.TestTask.BusinessFacade.Services
{
	public class CsvFileUploadService : IFileUploadService
	{
		public List<Transaction> ParseFile(Stream stream)
		{
			var file = new StreamReader(stream);
			List<Transaction> result = new List<Transaction>();

			var context = new CsvContext();
			var items = context.Read<CsvFileModel>(
				file,
				new CsvFileDescription
					{
						FileCultureName = "en-EN",
						QuoteAllFields = true,
						SeparatorChar = ',',
						FirstLineHasColumnNames = false,
						EnforceCsvColumnAttribute = true,
						UseOutputFormatForParsingCsvValue = true
					}).ToList();

			if (items.Count > 0)
			{
				ValidateModels(items);

				result = items.Select(
					p => new Transaction
							{
								TransactionId = p.TransactionId,
								Amount = p.Amount,
								CurrencyCode = p.CurrencyCode,
								TransactionDate = p.TransactionDate,
								Status = ConvertToDbStatus(p.Status)
							}).ToList();
			}

			return result;
		}

		private static void ValidateModels(IList<CsvFileModel> items)
		{
			CsvTransactionValidator validator = new CsvTransactionValidator();
			var errors = items
				.Select(p => new { Id = p.TransactionId, ValidateResult = validator.Validate(p) })
				.Where(p => !p.ValidateResult.IsValid)
				.SelectMany(p => p.ValidateResult.Errors.Select(e => p.Id + ": " + e.ErrorMessage))
				.ToList();
			if (errors.Any())
			{
				throw new ApplicationException(string.Join(", ", errors));
			}
		}

		private string ConvertToDbStatus(CsvTransactionStatus status)
		{
			TransactionStatus result;
			switch (status)
			{
				case CsvTransactionStatus.Approved:
					result = TransactionStatus.Approved;
					break;
				case CsvTransactionStatus.Failed:
					result = TransactionStatus.Rejected;
					break;
				case CsvTransactionStatus.Finished:
					result = TransactionStatus.Done;
					break;
				default:
					throw new ApplicationException("Failed row");
			}

			return result.ToString();
		}
	}
}