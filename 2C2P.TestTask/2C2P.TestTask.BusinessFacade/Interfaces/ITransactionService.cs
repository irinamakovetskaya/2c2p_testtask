﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using _2C2P.TestTask.BusinessFacade.Models;
using _2C2P.TestTask.BusinessFacade.Models.Enums;

namespace _2C2P.TestTask.BusinessFacade.Interfaces
{
	public interface ITransactionService
	{
		Task<string> UploadAsync(HttpPostedFileBase file);

		Task<List<ResponseItem>> GetByCurrency(string currencyCode);

		Task<List<ResponseItem>> GetByDateRange(DateTime dateFrom, DateTime dateTo);

		Task<List<ResponseItem>> GetByStatus(TransactionStatus status);
	}
}