﻿using System.Collections.Generic;
using System.IO;
using _2C2P.TestTask.DataAccess.Entities;

namespace _2C2P.TestTask.BusinessFacade.Interfaces
{
	public interface IFileUploadService
	{
		List<Transaction> ParseFile(Stream stream);
	}
}