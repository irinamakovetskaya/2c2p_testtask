﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using _2C2P.TestTask.DataAccess.Entities;

namespace _2C2P.TestTask.DataAccess.Interfaces
{
	public interface ITransactionRepository
	{
		Task AddAsync(List<Transaction> transactions);

		Task<List<Transaction>> Find(Expression<Func<Transaction, bool>> expression);
	}
}