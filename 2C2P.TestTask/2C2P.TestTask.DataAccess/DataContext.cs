using System.Data.Entity;
using _2C2P.TestTask.DataAccess.Entities;

namespace _2C2P.TestTask.DataAccess
{
	public partial class DataContext : DbContext
	{
		public DataContext()
			: base("name=DataContext")
		{
			var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
			Database.SetInitializer<DataContext>(null);
		}

		public virtual DbSet<DbTransaction> Transactions { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
		}
	}
}
