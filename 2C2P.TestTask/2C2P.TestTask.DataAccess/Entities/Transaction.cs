﻿using System;

namespace _2C2P.TestTask.DataAccess.Entities
{
	public class Transaction
	{
		public Transaction()
		{
		}

		public Transaction(Transaction transaction)
		{
			TransactionId = transaction.TransactionId;
			TransactionDate = transaction.TransactionDate;
			Amount = transaction.Amount;
			CurrencyCode = transaction.CurrencyCode;
			Status = transaction.Status;
		}

		public string TransactionId { get; set; }

		public decimal Amount { get; set; }

		public string CurrencyCode { get; set; }

		public DateTime TransactionDate { get; set; }

		public string Status { get; set; }

	}
}