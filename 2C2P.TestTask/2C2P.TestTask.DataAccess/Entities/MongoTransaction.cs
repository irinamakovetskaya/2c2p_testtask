﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace _2C2P.TestTask.DataAccess.Entities
{
	public class MongoTransaction : Transaction
	{
		public MongoTransaction()
			: base()
		{
		}

		public MongoTransaction(Transaction transaction) : base(transaction)
		{
			MongoId = ObjectId.GenerateNewId();
		}

		[BsonId]
		public ObjectId MongoId { get; set; }
	}
}
