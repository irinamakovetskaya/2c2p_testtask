﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace _2C2P.TestTask.DataAccess.Entities
{
	[Table("Transactions")]
	public class DbTransaction : Transaction
	{
		public DbTransaction()
			: base()
		{
		}

		public DbTransaction(Transaction transaction)
			: base(transaction)
		{
		}

		public Int64 Id { get; set; }
	}
}