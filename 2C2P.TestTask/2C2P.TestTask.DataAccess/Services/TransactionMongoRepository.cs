﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using _2C2P.TestTask.DataAccess.Entities;
using _2C2P.TestTask.DataAccess.Extensions;
using _2C2P.TestTask.DataAccess.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;

namespace _2C2P.TestTask.DataAccess.Services
{
	public class TransactionMongoRepository : ITransactionRepository
	{
		private readonly IMongoCollection<MongoTransaction> _transactions;

		public TransactionMongoRepository()
		{
			var client = new MongoClient(ConfigurationManager.ConnectionStrings["MongoDB"].ToString());
			var database = client.GetDatabase("test");
			_transactions = database.GetCollection<MongoTransaction>("transactions");
		}

		public async Task AddAsync(List<Transaction> transactions)
		{
			if (transactions != null)
			{
				var mongoTransactions = transactions.Select(p => new MongoTransaction(p));
				await _transactions.InsertManyAsync(mongoTransactions);
			}
		}

		public async Task<List<Transaction>> Find(Expression<Func<Transaction, bool>> expression)
		{
			var mongoExpression = expression.ReplaceParameter<Transaction, MongoTransaction>();
			var mongoTransactions = await _transactions.Find(mongoExpression).ToListAsync();
			return mongoTransactions.Select(p => (Transaction)p).ToList();
		}
	}
}