﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using _2C2P.TestTask.DataAccess.Entities;
using _2C2P.TestTask.DataAccess.Extensions;
using _2C2P.TestTask.DataAccess.Interfaces;

namespace _2C2P.TestTask.DataAccess.Services
{
	public class TransactionRepository : ITransactionRepository
	{
		private readonly DataContext _dbContext;

		public TransactionRepository()
		{
			_dbContext = new DataContext();
		}

		public Task AddAsync(List<Transaction> transactions)
		{
			_dbContext.Transactions.AddRange(transactions.Select(p => new DbTransaction(p)));
			return _dbContext.SaveChangesAsync();
		}

		public async Task<List<Transaction>> Find(Expression<Func<Transaction, bool>> exp)
		{
			var expression = exp.ReplaceParameter<Transaction, DbTransaction>();
			var query = _dbContext.Transactions.Where(expression);
			var transactions = query.Select(p => (Transaction)p).ToList();

			return transactions;
		}
	}
}