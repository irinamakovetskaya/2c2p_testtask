﻿using System.Web.Mvc;
using System.Web.Routing;
using _2C2P.TestTask.BusinessFacade;

namespace _2C2P.TestTask.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			var _containerPolicy = new ContainerPolicy();
			_containerPolicy.Init();
		}
	}
}
