﻿using System.Web.Mvc;
using System.Web.Routing;

namespace _2C2P.TestTask.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "api",
				url: "api/transaction/{action}",
				defaults: new { controller = "Transaction", action = "ByCurrency" });
				
			routes.MapRoute(
					name: "Default",
					url: "{controller}/{action}",
					defaults: new { controller = "File", action = "Index" });
		}
	}
}