﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using _2C2P.TestTask.BusinessFacade.Extensions;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using LINQtoCSV;
using NLog;

namespace _2C2P.TestTask.Web.Controllers
{
	public class FileController : Controller
	{
		private readonly ITransactionService _transactionService;
		private readonly Logger _logger;

		public FileController(ITransactionService transactionService)
		{
			_transactionService = transactionService;
			_logger = LogManager.GetCurrentClassLogger();
		}

		[HttpGet]
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Upload(HttpPostedFileBase file)
		{
			try
			{
				ViewBag.UploadError = await _transactionService.UploadAsync(file);
			}
			catch (Exception exception)
			{
				_logger.Error(exception.Message);
				if (exception.InnerException != null)
				{
					_logger.Error(exception.InnerException.Message);
				}

				if (exception is AggregatedException)
				{
					_logger.Error(((AggregatedException)exception).GetAllMessages());
				}

				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, exception.Message);
			}

			return View("Index");
		}
	}
}