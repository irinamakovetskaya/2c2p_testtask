﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using _2C2P.TestTask.BusinessFacade.Interfaces;
using _2C2P.TestTask.BusinessFacade.Models.Enums;

namespace _2C2P.TestTask.Web.Controllers
{
    public class TransactionController : Controller
	{
		private readonly ITransactionService _transactionService;

		public TransactionController(ITransactionService transactionService)
		{
			_transactionService = transactionService;
		}

		public async Task<JsonResult> ByCurrency(string currencyCode)
		{
			var result = await _transactionService.GetByCurrency(currencyCode);

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public async Task<JsonResult> ByDateRange(DateTime dateFrom, DateTime dateTo)
		{
			var result = await _transactionService.GetByDateRange(dateFrom, dateTo);

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public async Task<JsonResult> ByStatus(TransactionStatus status)
		{
			var result = await _transactionService.GetByStatus(status);

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}
