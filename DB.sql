CREATE DATABASE 2C2P.TestTask;
GO
USE [2C2P.TestTask]
Create table [dbo].[Transactions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionId] [nvarchar](50) NOT NULL,
	[Amount] [money] NOT NULL,
	[CurrencyCode] [nvarchar](3) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Status] [nvarchar](20) NOT NULL
) ON [PRIMARY]